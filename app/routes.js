/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import HomePage from './containers/HomePage';
import WebView from './containers/WebView';
import imProtocols from './imProtocols';

function ImRoute({ url, title, colour }) {
  return (
    <Route
      key={title}
      path={`/im/${title.toLowerCase()}`}
    >
      {({ match }) => (
        <WebView
          isActive={!!match}
          url={url}
          colour={colour}
          protocol={title}
        />
      )}
    </Route>
  );
}
ImRoute.propTypes = {
  url: PropTypes.string,
  title: PropTypes.string,
  colour: PropTypes.string,
};

export default () => (
  <div style={{
 width: '100%', flexGrow: 1, display: 'flex', flexDirection: 'column'
}}>
    <Route path="/" exact component={HomePage} />
    {imProtocols.map(ImRoute)}
  </div>
);
