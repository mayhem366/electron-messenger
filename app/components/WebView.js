import path from 'path';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ProgressCircle } from 'react-desktop';
import cn from 'classnames';
import { remote } from 'electron';

import styles from './WebView.css';


const preloadPath = process.env.NODE_ENV === 'production' ?
  path.join(remote.app.getAppPath(), 'dist', 'attachNotifications.js') :
  path.join(__dirname, 'attachNotifications.js');
console.log('Preload file is location at', preloadPath);

class WebView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      hasBeenActive: props.isActive
    };
    this.hasEventHandlers = false;
  }


  componentDidMount() {
    if (!this.hasEventHandlers && this.webview) {
      this.webview.addEventListener('did-start-loading', this.onStartLoading);
      this.webview.addEventListener('did-stop-loading', this.onStopLoading);
      this.webview.addEventListener('dom-ready', this.onDomReady);
      this.hasEventHandlers = true;
    }

    if (this.props.isActive) {
      this.props.switchProtocol(this.props.protocol);
    }
  }


  componentWillReceiveProps(nextProps) {
    if (!this.props.isActive && nextProps.isActive) {
      this.props.switchProtocol(this.props.protocol);

      if (!this.state.hasBeenActive) {
        this.setState({ hasBeenActive: true });
      }
    }
  }

  componentDidUpdate() {
    if (!this.hasEventHandlers) {
      this.webview.addEventListener('did-start-loading', this.onStartLoading);
      this.webview.addEventListener('did-stop-loading', this.onStopLoading);
      this.webview.addEventListener('dom-ready', this.onDomReady);
      this.hasEventHandlers = true;
    }
  }

  componentWillUnmount() {
    console.log('Unmounted webview');

    if (this.webview) {
      this.webview.removeEventListener('did-start-loading', this.onStartLoading);
      this.webview.removeEventListener('did-stop-loading', this.onStopLoading);
      this.webview.removeEventListener('dom-ready', this.onStopLoading);
    }
  }

  onDomReady = () => {
    this.webview.executeJavaScript(`window.attachNotifications('${this.props.protocol}');`);

    if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
      console.log('Opening dev tools for webview');
      this.webview.openDevTools();
    }
  }

  onStartLoading = () => {
    this.setState({ isLoading: true });
  }

  onStopLoading = () => {
    this.setState({ isLoading: false });
  }

  render() {
    const { isLoading, hasBeenActive } = this.state;
    if (!hasBeenActive) return null;

    const { url, colour, isActive } = this.props;

    return (
      <div className={cn(styles.container, {
        [styles.active]: isActive
      })}
      >
        {isLoading && <ProgressCircle className={styles.loader} size={100} color={colour} />}

        <webview
          ref={(ref) => { this.webview = ref; }}
          className={cn(styles.webview, {
            [styles.loading]: isLoading
          })}
          preload={preloadPath}
          src={url}
        />
      </div>
    );
  }
}

WebView.propTypes = {
  url: PropTypes.string.isRequired,
  colour: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  switchProtocol: PropTypes.func.isRequired,
  protocol: PropTypes.string.isRequired,
};

export default WebView;
