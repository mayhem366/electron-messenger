import React from 'react';
import PropTypes from 'prop-types';

const NotificationSvg = ({ svg, className }) => (
  <svg viewBox="0 0 50 50" className={className}>
    {svg}

    <circle cx="38" cy="38" r="10" fill="red" stroke="black" strokeWidth="1" />
  </svg>
);

NotificationSvg.propTypes = {
  svg: PropTypes.node.isRequired,
  className: PropTypes.string,
};
NotificationSvg.defaultProps = {
  className: undefined
};

export default NotificationSvg;
