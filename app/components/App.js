import React, { Component } from 'react';
import TitleBar from '../containers/TitleBar';
import NavPane from '../components/NavPane';
import styles from './App.css';
import Routes from '../routes';
import imProtocols from '../imProtocols';

export default class App extends Component {
  render() {
    return (
      <div className={styles.container}>

        <div className={styles.titleWrapper}>
          <TitleBar />
        </div>

        <NavPane
          className={styles.navPane}
          defaultIsPaneExpanded={false}
          navItems={imProtocols}
        >

          <Routes />
        </NavPane>
      </div>
    );
  }
}
