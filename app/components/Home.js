import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import imProtocols from '../imProtocols';

const firstProtocol = imProtocols[0].title.toLowerCase();

export default class Home extends Component {
  render() {
    return <Redirect to={`/im/${firstProtocol}`} />;
  }
}
