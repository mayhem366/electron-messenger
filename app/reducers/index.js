// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { SWTICH_IM_PROTOCOL } from '../actions/imProtocol';

const rootReducer = combineReducers({
  router,
  imProtocol: (state = null, action) => {
    if (action.type === SWTICH_IM_PROTOCOL) { return action.payload; }

    return state;
  }
});

export default rootReducer;
