import { connect } from 'react-redux';
import webview from '../components/WebView';
import switchProtocol from '../actions/imProtocol';

const mapStateToProps = () => ({

});

const mapDispatchToProps = {
  switchProtocol
};

export default connect(mapStateToProps, mapDispatchToProps)(webview);
