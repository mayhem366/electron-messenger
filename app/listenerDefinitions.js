module.exports = {
  Slack: {
    hasMessage: (doc) => doc.getElementById('col_channels')
      .querySelectorAll('.p-channel_sidebar__channel--unread:not(.p-channel_sidebar__channel--muted)').length > 0,
    watchNodeSelector: '#col_channels'
  },

  Messenger: {
    watchNodeSelector: "[aria-label='Conversation list']",
    hasMessage: (doc) => doc.querySelector("[aria-label='Conversation list']")
      .querySelectorAll("[aria-live='polite']").length > 0
  },

  WhatsApp: {
    watchNodeSelector: '#pane-side',
    hasMessage: (doc) => doc.getElementById('pane-side')
      .querySelectorAll('.chat.unread').length > 0
  }
};
