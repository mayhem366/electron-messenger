const ipc = require('electron').ipcRenderer;
const protocols = require('./listenerDefinitions');
require('arrive');


document.addEventListener('click', (e) => {
  const anchor = getClickedAnhor(e.target);
  if (!anchor || anchor.target !== '_blank') return;

  ipc.send('link-clicked', {
    url: anchor.href
  });
// e.preventDefault();
});

function getClickedAnhor(node) {
  if (node.tagName === 'A') return node;

  if (node.parentNode) {
    return getClickedAnhor(node.parentNode);
  }

  return null;
}


// const url = new URL(window.location.href);
// console.log(__filename);
// const protocol = url.searchParams.get('protocol');
// console.log(`Running the webview preload script for protocol ${protocol}`);

window.attachNotifications = function attachNotifications(protocol) {
  console.log(`Attaching notifications for protocol ${protocol}`);

  // notificationShim();

  const listenerDefinition = protocols[protocol];
  console.log(listenerDefinition);
  if (!listenerDefinition) return;


  console.log('Content loaded');
  //   new MutationObserver((mutations) => {
  //     const newTitle = mutations[0].addedNodes[0].data;
  //     ipc.send('webview-title-changed', { title: newTitle, protocol });
  //   }).observe(
  //     document.querySelector('title'),
  //     {
  //       attributes: true,
  //       childList: true,
  //       subtree: true,
  //       characterData: true
  //     }
  // );

  document.arrive(
    listenerDefinition.watchNodeSelector, { onceOnly: true, existing: true },
    // eslint-disable-next-line func-names
    function () {
      const elementToWatch = this;

      if (elementToWatch) {
        new MutationObserver((mutations) => {
          const hasMessage = listenerDefinition.hasMessage(document);
          console.log(mutations);
          console.log('Change detected, has a message?', hasMessage);
          if (hasMessage) {
            ipc.send('notification-shim', {
              protocol
            });
          } else {
            ipc.send('msg-cleared', protocol);
          }
        }).observe(elementToWatch, {
          childList: true,
          subtree: true,
          attributes: true,
          characterData: true
        });
      }
    }
  );

  // Remove the handler for arrival events to cut down on performance
  setTimeout(document.unbindArrive, 5000);


//   function notificationShim() {
//     const OldNotification = Notification;
//
//   /* eslint-disable no-global-assign */
//     Notification = function (title, options) {
//       ipc.send('notification-shim', {
//         title,
//         options,
//         protocol
//       });
//
//       // Send the native Notification.
//       // You can't catch it, that's why we're doing all of this. :)
//       return new OldNotification(title, options);
//     };
//
//     Notification.prototype = OldNotification.prototype;
//     Notification.permission = OldNotification.permission;
//     Notification.requestPermission = OldNotification.requestPermission;
//   }
};
