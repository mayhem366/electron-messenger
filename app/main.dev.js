/* eslint global-require: 0 */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `npm run build` or `npm run build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 */
import { app, BrowserWindow, ipcMain } from 'electron';
import opn from 'opn';

let mainWindow = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
  require('electron-debug')();
  const path = require('path');
  const p = path.join(__dirname, '..', 'app', 'node_modules');
  require('module').globalPaths.push(p);
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = [
    'REACT_DEVELOPER_TOOLS',
    'REDUX_DEVTOOLS'
  ];

  return Promise
    .all(extensions.map(name => installer.default(installer[name], forceDownload)))
    .catch(console.log);
};


/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});


app.on('ready', async () => {
  if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
    await installExtensions();
  }

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    frame: false
  });

  mainWindow.setMenu(null);

  // Listen for notification events.
  ipcMain.on('notification-shim', (e, msg) => {
    mainWindow.webContents.send('msg-received', msg.protocol);
    mainWindow.flashFrame(true);
  });

  ipcMain.on('msg-cleared', (e, msg) => {
    mainWindow.webContents.send('msg-cleared', msg);
  });

  ipcMain.on('tab-changed', () => {
    // No need to do anything if maximised, the size won't have changed
    if (mainWindow.isMaximized()) return;

    // HACK: Force the window to resize by a pixel so that the newly active webview
    //  takes the correct size as the window may have been resized when it was inactive
    const size = mainWindow.getContentSize();
    mainWindow.setContentSize(size[0] + 1, size[1] + 1);
    mainWindow.setContentSize(size[0], size[1]);
  });

  ipcMain.on('windowstate-modification-requested', (e, msg) => {
    switch (msg) {
      case 'minimize':
        mainWindow.minimize();
        break;
      case 'maximize':
        if (mainWindow.isMaximized()) {
          mainWindow.unmaximize();
        } else {
          mainWindow.maximize();
        }
        break;
      case 'close':
        mainWindow.close();
        break;
      default:
        throw new Error('Unsupported window state change');
    }
  });

  ipcMain.on('link-clicked', (e, { url }) => {
    opn(url);
  });

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    mainWindow.show();
    mainWindow.focus();

    if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
      mainWindow.webContents.openDevTools();
    }
  });

  mainWindow.on('unmaximize', () => {
    mainWindow.webContents.send('windowstate-modified', 'restored');
  });

  mainWindow.on('maximize', () => {
    mainWindow.webContents.send('windowstate-modified', 'maximized');
  });

  mainWindow.on('focus', () => {
    // mainWindow.webContents.send('window-focussed');
    mainWindow.flashFrame(false);
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
});
